<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <link href='http://fonts.googleapis.com/css?family=Cantarell' rel='stylesheet' type='text/css'>
</head>
<body class="<?php print $body_classes; ?>">

  <div id="bg-wrapper">
    <div id="right" class="right">
    
      <?php if ($site_slogan): ?>
        <div id="site-slogan">
          <?php print $site_slogan; ?>
          </div>
      <?php endif; ?>
    
      <?php if ($mission): ?>
        <div id="mission">
          <?php print $mission; ?>
        </div>
      <?php endif; ?>
      
      <?php if ($secondary_links): ?>
        <div id="secondary-links">
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        </div>
      <?php endif; ?>
    
      <?php if ($right): ?>
        <?php print $right; ?>
      <?php endif; ?>
    
    </div>
  
    <div id="content">
      <?php if ($primary_links): ?>
        <div id="primary-links" class="right">
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        </div>
      <?php endif; ?>
      
      <a href="<?php print $base_path; ?>"><h1 id="page-title"><?php print $site_name; ?></h1></a>
      <div id="main-content">
        <?php print $help; ?>
        <?php print $tabs; ?>
        <?php print $messages; ?>
    
        <?php if ($content_top): ?>
          <?php print $content_top; ?>
        <?php endif; ?>
    
        <?php print $content; ?>
        
        <?php if ($footer_message): ?>
          <div id="footer">
            <?php print $footer_message; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  <div>
<?php print $closure; ?>
</body>
</html>