<?php

/*
 * Implementation of template_preprocess_node()
 */
function sproing_preprocess_node(&$variables){
  $node = $variables['node'];
  $variables['date'] = format_date($node->created, 'custom', 'l, F j, Y - h:i A');
  $variables['blog_name'] = l($node->links['blog_usernames_blog']['title'], $node->links['blog_usernames_blog']['href'], array('attributes' => array('title' => $node->links['blog_usernames_blog']['attributes']['title'])));
  $node->links['comment_add']['title'] = 'Comment!';
  $node->links['node_read_more']['title'] = 'Keep reading >>';
  $variables['comment_link'] = l($node->links['comment_add']['title'], $node->links['comment_add']['href'], array('attributes' => array('title' => $node->links['comment_add']['attributes']['title'])));
  $variables['read_more'] = l($node->links['node_read_more']['title'], $node->links['node_read_more']['href'], array('attributes' => array('title' => $node->links['node_read_more']['attributes']['title'])));
  $variables['comment_comments'] = l($node->links['comment_comments']['title'], $node->links['comment_comments']['href'], array('attributes' => array('title' => $node->links['comment_comments']['attributes']['title'])));
  
  if ($node->comment_count > 0) {
    $variables['page_tools_comment_link'] = $variables['comment_comments'];
  } else {
    $variables['page_tools_comment_link'] = $variables['comment_link'];
  }
  
  if (!$node->teaser) {
    $variables['page_tools_comment_link'] = $variables['comment_link'];
  }
}

/* 
 * Implementation of theme_links()
 */
function sproing_links($links, $attributes = array('class' => 'links')) {
  global $language;
  $output = '';

  if (count($links) > 0) {
    if ($attributes['class'] == 'links secondary-links') {
      $output = '<ul class="secondary-links">';
    } else {
      $output = '<ul'. drupal_attributes($attributes) .'>';
    }

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}
