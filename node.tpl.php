<?php // $Id$ ?>
<div id="node-<?php print $node->nid; ?>" class="post node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
	<h1 class="title"><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h1>
	
	<p class="byline">
	  By <?php print ($name); ?>
	  <?php if ($blog_name): ?>
	    in <?php print $blog_name; ?>
	  <?php endif; ?>
	</p>
	
	<p class="date"><?php print $date; ?></p>
	
	<div class="entry">
		<?php print $content; ?>
	</div>

	<div class="meta">
	<?php if ($taxonomy && $teaser): ?>
     <?php print $terms; ?><br/><br/>
  <?php endif;?>
  </div>

  <div class="post-tools">
    <?php if ($page_tools_comment_link): ?>
      <?php print $page_tools_comment_link; ?>
    <?php endif; ?>
    <?php if ($read_more): ?>
      <?php print $read_more; ?>
    <?php endif; ?>
  </div>
</div>
