<?php // $Id$ ?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">

  <?php if ($comment->new) : ?>
    <span class="new"><?php print drupal_ucfirst($new) ?></span>
  <?php endif; ?>
  <?php print $picture ?>

    <h3><?php print $title ?></h3>
    <?php if ($submitted): ?>
      <p class="date"><?php print $submitted; ?></p>
    <?php endif; ?>

    <div class="content">
      <?php print $content ?>
      <?php if ($picture): ?>
        <div class="right">
          <?php print $picture; ?>
        </div>
      <?php endif; ?>
      <?php if ($signature): ?>
      <div class="clear-block">
        <div>—</div>
        <?php print $signature ?>
      </div>
      <?php endif; ?>
    </div>

  <?php if ($links): ?>
    <div class="links"><?php print $links ?></div>
  <?php endif; ?>
</div>
